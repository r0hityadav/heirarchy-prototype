var APP;

APP = APP || {};

APP.helpers = {

    createNodes: function(data) {
        var tree = $('.tree');

        if (tree.length) {
            processNode(data, tree)
        }

        function processNode(data, parentNode) {
            var newParentNode;

            if(data.name && data.uid){
                newParentNode = APP.helpers.addNode(parentNode, {
                    content: data.name,
                    id: data.uid
                });

                if(data.children && data.children.length){
                    processChildren(data.children, newParentNode);
                }
            }
        }

        function processChildren(data, parentNode) {
            for (var i = 0, len = data.length; i < len; i++) {
                processNode(data[i], parentNode);
            }
        }
    },
    findNode: function(nodeId) {
        var nodeFound;
        processNode(APP.nodes);
        return nodeFound;

        function processNode(data) {
            if(data.uid === nodeId){
                nodeFound = data;
            }
            if (data.children && data.children.length){
                processChildren(data.children);
            }
        }

        function processChildren(data) {
            for (var i = 0, len = data.length; i < len; i++) {
                if(nodeFound){
                    break;
                }
                processNode(data[i]);
            }
        }
    },
    addNewNode: function(domNode, data) {
        var node = APP.helpers.findNode(domNode.attr('id')),
            nodeChildrenIdPrefix = node.uid;

        if (!node.children) {
            node.children = [];
        }
        var childNode = {
            name: data,
            uid: nodeChildrenIdPrefix + '.' + (node.children.length + 1)
        };
        
        childNode.pathDepth = APP.helpers.getPathDepth(childNode.uid);

        node.children.push(childNode);
        //Add to the li instead of the anchor tag
        APP.helpers.addNode(domNode.parent(), {
            content: childNode.name,
            id: childNode.uid
        });

         $('.json-content').html(APP.helpers.syntaxHighlighter(JSON.stringify(APP.nodes, null, 4)));
    },

    addNode: function(el, nodeVal) {
        var ul = $('<ul class="cf"></ul>'),
            li = $('#nodeTemplate').html(),
            $li = $(li),
            $existingUl = el.find('>ul');

        $li.find('a').html(nodeVal.content).attr('id', nodeVal.id);

        if ($existingUl.length === 1) {
            $existingUl.append($li);
        } else if (el.next('ul').length === 1) {
            el.next('ul').append($li);
        } else {
            el.append(ul.append($li));
        }
        return $li;
    },

    syntaxHighlighter: function (json) {
        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function(match) {
            var cls = 'number';
            if (/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = 'key';
                } else {
                    cls = 'string';
                }
            } else if (/true|false/.test(match)) {
                cls = 'boolean';
            } else if (/null/.test(match)) {
                cls = 'null';
            }
            return '<span class="' + cls + '">' + match + '</span>';
        });
    },

    getPathDepth: function (uid) {
        return uid.replace(/\d/g, '').length;
    }

};


$(function() {
    var $nodeNameBox = $('#jsNodeName');

    if (APP.nodes) {
        APP.helpers.createNodes(APP.nodes);
        $('.json-content').html(APP.helpers.syntaxHighlighter(JSON.stringify(APP.nodes, null, 4)));
    }

    $('.tree').on('nodeSelected', function(e, node) {
        if (node.selected) {
            $('#jsAddChild').removeAttr('disabled');
        } else {
            $('#jsAddChild').prop('disabled', 'disabled');
        }
    });

    $('#jsAddChild').on('click', function(e) {
        e.preventDefault();
        APP.helpers.addNewNode($('.selected'), $nodeNameBox.val());
    });

    $('#jsShowJSON').on('click', function(e) {
        e.preventDefault();
        $(this).addClass('hide');
        $('.json').removeClass('hide');
        $('#jsHideJSON').removeClass('hide');
    });

    $('#jsHideJSON').on('click', function(e) {
        e.preventDefault();
        $(this).addClass('hide');
        $('.json').addClass('hide');
        $('#jsShowJSON').removeClass('hide');
    });

    $('#jsExpandJson').on('click', function(e) {
        e.preventDefault();
        $(this).addClass('hide');
        $('.json').addClass('expanded');
        $('#jsCollapseJson').removeClass('hide');
    })

    $('#jsCollapseJson').on('click', function(e) {
        e.preventDefault();
        $(this).addClass('hide');
        $('.json').removeClass('expanded');  
        $('#jsExpandJson').removeClass('hide');
    })

    $('.tree').on('click', 'a', function() {
        var $this = $(this);
        if ($this.hasClass('selected')) {
            $this.removeClass('selected');
            $('.tree').trigger('nodeSelected', {
                node: this,
                selected: false
            });
        } else {
            $('.tree a').removeClass('selected');
            $(this).addClass('selected');
            $('.tree').trigger('nodeSelected', {
                node: this,
                selected: true
            });
        }
    });
});
